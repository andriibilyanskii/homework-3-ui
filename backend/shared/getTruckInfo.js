/**
 * @constructor
 * @param {Array} trucks
 */
function getTrucksInfo(trucks) {
  return trucks.map((e) => {
    return {
      _id: e._id,
      created_by: e.created_by,
      assigned_to: e.assigned_to,
      type: e.type,
      status: e.status,
      created_date: e.created_date,
    };
  });
}

module.exports = getTrucksInfo;
