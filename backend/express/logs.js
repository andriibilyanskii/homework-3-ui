const Logs = require('./model/logs');
/**
 * @constructor
 * @param {string} req - request
 * @param {string} res - response
 */
async function createLog(req, res) {
  const date = new Date().toISOString();
  let logText = `${req.method}\t${req.path}\t${date}`;

  if (req.method === 'PUT' || req.method === 'POST') {
    logText += '\t' + JSON.stringify(req.body);
  }

  console.log(logText);

  try {
    const log = new Logs({
      log: {
        method: req.method,
        path: req.path,
        date: date,
        body: JSON.stringify(req.body),
      },
    });
    await log.save();
  } catch (e) {}
}

module.exports = createLog;
