const express = require('express');
const changeState = require('./load/changeState');
const router = new express.Router();
const createNote = require('./load/createNote');
const deleteNote = require('./load/deleteNote');
const getNote = require('./load/getNote');
const getNotes = require('./load/getNotes');
const updateNote = require('./load/updateNote');

router.get('/api/notes', (req, res) => {
  getNotes(req, res);
});

router.post('/api/notes', (req, res) => {
  createNote(req, res);
});

router.get('/api/notes/:id', (req, res) => {
  getNote(req, res);
});

router.put('/api/notes/:id', (req, res) => {
  updateNote(req, res);
});

router.patch('/api/notes/:id', (req, res) => {
  changeState(req, res);
});

router.delete('/api/notes/:id', (req, res) => {
  deleteNote(req, res);
});

module.exports = router;
