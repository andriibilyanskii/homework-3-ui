const express = require('express');
const profileInfo = require('./users/profileInfo');
const deleteProfile = require('./users/deleteProfile');
const changePassword = require('./users/changePassword');
const router = new express.Router();

router.get('/api/users/me', (req, res) => {
  profileInfo(req, res);
});

router.delete('/api/users/me', (req, res) => {
  deleteProfile(req, res);
});

router.patch('/api/users/me/password', (req, res) => {
  changePassword(req, res);
});

module.exports = router;
