const Users = require('../../model/users');
const Credentials = require('../../model/credentials');
const {errorObj} = require('../../model/error');
const RegistrationCredentials = require('../../model/registrationCredentials');

/**
 * @constructor
 * @param {string} req - request
 * @param {string} res - response
 */
async function deleteProfile(req, res) {
  try {
    if (!req.user.email) {
      throw Error();
    }
    const user = await Users.findOne({email: req.user.email});
    if (user && !user.assignedLoad) {
      await Credentials.deleteOne({email: req.user.email});
      await RegistrationCredentials.deleteOne({email: req.user.email});
      await Users.deleteOne({email: req.user.email});
      res.status(200).send({message: 'Profile deleted successfully'});
    } else {
      res.status(400).send(errorObj);
    }
  } catch (e) {
    res.status(500).send(errorObj);
  }
}

module.exports = deleteProfile;
