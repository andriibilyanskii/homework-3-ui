const Loads = require('../../model/loads');
const Users = require('../../model/users');
const getLoadsInfo = require('../../../shared/getLoadsInfo');
const {errorObj} = require('../../model/error');

/**
 * @constructor
 * @param {string} req - request
 * @param {string} res - response
 */
async function getActiveLoad(req, res) {
  try {
    if (!Loads || req.user.role !== 'DRIVER') {
      throw new Error();
    }

    const driver = await Users.findOne({
      email: req.user.email,
    });

    const loads = await Loads.find({_id: driver.assignedLoad});
    if (loads.length === 0) {
      res.status(400).send(errorObj);
    } else {
      res.status(200).send({
        load: getLoadsInfo(loads)[0],
      });
    }
  } catch (e) {
    console.log(e);
    res.status(500).send(errorObj);
  }
}

module.exports = getActiveLoad;
