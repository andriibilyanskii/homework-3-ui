const Loads = require('../../model/loads');
const Trucks = require('../../model/truck');
const {errorObj} = require('../../model/error');
const logsForLoad = require('../../../shared/logsForLoad');
const truckTypes = require('../../../shared/truckTypes');
const loadState = require('./loadState');
const Users = require('../../model/users');
const RegistrationCredentials = require('../../model/registrationCredentials');

/**
 * @constructor
 * @param {string} req - request
 * @param {string} res - response
 */
async function postLoad(req, res) {
  const id = req.params.id;

  try {
    if (req.user.role.toUpperCase() !== 'SHIPPER') {
      throw new Error();
    }

    const load = await Loads.findOne({
      _id: id,
    });

    if (load && !load.assigned_to) {
      await Loads.updateOne(
          {_id: id},
          {
            $set: {
              status: 'POSTED',
              logs: logsForLoad(load.logs, 'Status is changed to POSTED'),
            },
          },
      );

      let truck = await Trucks.find({
        assigned_to: /^(?!\s*$).+/,
        status: 'IS',
      });
      truck = truck.filter((e) => {
        return (
          load.payload < truckTypes[e.type].payload &&
          load.dimensions.width < truckTypes[e.type].width &&
          load.dimensions.height < truckTypes[e.type].height &&
          load.dimensions.length < truckTypes[e.type].length
        );
      })[0];

      if (!truck) {
        await Loads.updateOne(
            {_id: id},
            {
              $set: {
                status: 'NEW',
                logs: logsForLoad(load.logs, 'Status is changed to NEW'),
              },
            },
        );
        res.status(200).send({
          message: `Load isn't posted`,
          driver_found: false,
        });
      } else {
        await Loads.updateOne(
            {_id: id},
            {
              $set: {
                assigned_to: truck.assigned_to,
                status: 'ASSIGNED',
                state: loadState.enRoutePickUp,
                logs: logsForLoad(
                    load.logs,
                    'Load assigned to driver with id ' + truck.assigned_to,
                ),
              },
            },
        );

        await Trucks.updateOne(
            {_id: truck._id},
            {
              $set: {
                status: 'OL',
              },
            },
        );

        const driver = await RegistrationCredentials.findOne({
          _id: truck.assigned_to,
        });
        await Users.updateOne(
            {email: driver.email},
            {$set: {assignedLoad: load._id}},
        );

        res.status(200).send({
          message: 'Load posted successfully',
          driver_found: true,
        });
      }
    } else {
      res.status(400).send(errorObj);
    }
  } catch (e) {
    console.log(e);
    res.status(500).send(errorObj);
  }
}

module.exports = postLoad;
