const Loads = require('../../model/loads');
const Trucks = require('../../model/truck');
const {errorObj} = require('../../model/error');
const getLoadsInfo = require('../../../shared/getLoadsInfo');
const getTrucksInfo = require('../../../shared/getTruckInfo');
const RegistrationCredentials = require('../../model/registrationCredentials');
const Users = require('../../model/users');

/**
 * @constructor
 * @param {string} req - request
 * @param {string} res - response
 */
async function shippingInfo(req, res) {
  const id = req.params.id;

  try {
    if (req.user.role.toUpperCase() !== 'SHIPPER') {
      throw new Error();
    }

    const load = await Loads.find({
      _id: id,
    });

    const driver = await RegistrationCredentials.findOne({
      _id: load[0].assigned_to,
    });

    const driverUser = await Users.findOne({
      email: driver.email,
    });

    const truck = await Trucks.find({
      _id: driverUser.assignedTruck,
    });

    if (load && !load.assigned_to && truck) {
      res.status(200).send({
        load: getLoadsInfo(load)[0],
        truck: getTrucksInfo(truck)[0],
      });
    } else {
      res.status(400).send(errorObj);
    }
  } catch (e) {
    res.status(500).send(errorObj);
  }
}

module.exports = shippingInfo;
