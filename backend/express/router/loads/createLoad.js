/* eslint-disable camelcase */
const Loads = require('../../model/loads');
const {errorObj} = require('../../model/error');

/**
 * @constructor
 * @param {string} req - request
 * @param {string} res - response
 */
async function createLoad(req, res) {
  try {
    const {name, payload, pickup_address, delivery_address, dimensions} =
      req.body;

    if (req.user.role.toUpperCase() !== 'SHIPPER') {
      throw new Error();
    }

    if (
      !name ||
      !payload ||
      !pickup_address ||
      !delivery_address ||
      !dimensions
    ) {
      res.status(400).send(errorObj);
      return;
    }

    const dateNow = new Date().toISOString();
    const load = new Loads({
      created_by: req.user.user_id,
      assigned_to: '',
      status: 'NEW',
      state: '',
      name,
      payload,
      pickup_address,
      delivery_address,
      dimensions,
      logs: [{
        message: 'Load is created',
        time: dateNow,
      }],
      created_date: dateNow,
    });
    await load.save().then(() =>
      res.status(200).send({
        message: 'Load created successfully',
      }),
    );
  } catch (e) {
    res.status(500).send(errorObj);
  }
}

module.exports = createLoad;
