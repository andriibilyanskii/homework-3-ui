/* eslint-disable camelcase */
const Loads = require('../../model/loads');
const {errorObj} = require('../../model/error');

/**
 * @constructor
 * @param {string} req - request
 * @param {string} res - response
 */
async function updateLoad(req, res) {
  try {
    const id = req.params.id;
    const {name, payload, pickup_address, delivery_address, dimensions} =
      req.body;

    if (
      !name ||
      !payload ||
      !pickup_address ||
      !delivery_address ||
      !dimensions
    ) {
      res.status(400).send(errorObj);
      return;
    }

    if (!id || req.user.role.toUpperCase() !== 'SHIPPER') {
      throw new Error();
    }

    const load = await Loads.findOne({_id: id});
    if (load && load.status === 'NEW') {
      await Loads.updateOne(
          {_id: id},
          {$set: {name, payload, pickup_address, delivery_address, dimensions}},
      );
      res.status(200).send({message: 'Load details changed successfully'});
    } else {
      res.status(400).send(errorObj);
    }
  } catch (e) {
    res.status(500).send(errorObj);
  }
}

module.exports = updateLoad;
