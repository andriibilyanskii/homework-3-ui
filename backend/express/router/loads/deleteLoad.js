const Loads = require('../../model/loads');
const {errorObj} = require('../../model/error');

/**
 * @constructor
 * @param {string} req - request
 * @param {string} res - response
 */
async function deleteLoad(req, res) {
  try {
    const id = req.params.id;

    if (!id || req.user.role.toUpperCase() !== 'SHIPPER') {
      throw new Error();
    }

    const load = await Loads.findOne({_id: id});
    if (load && load.status === 'NEW') {
      await Loads.deleteOne({_id: id});
      res.status(200).send({message: 'Load deleted successfully'});
    } else {
      res.status(400).send(errorObj);
    }
  } catch (e) {
    res.status(500).send(errorObj);
  }
}

module.exports = deleteLoad;
