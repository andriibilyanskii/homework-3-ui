const express = require('express');
const addTruck = require('./trucks/addTruck');
const assignTruck = require('./trucks/assignTruck');
const deleteTruck = require('./trucks/deleteTruck');
const getTruck = require('./trucks/getTruck');
const getTrucks = require('./trucks/getTrucks');
const updateInfo = require('./trucks/updateInfo');
const router = new express.Router();

router.get('/api/trucks', (req, res) => {
  getTrucks(req, res);
});

router.post('/api/trucks', (req, res) => {
  addTruck(req, res);
});

router.get('/api/trucks/:id', (req, res) => {
  getTruck(req, res);
});

router.put('/api/trucks/:id', (req, res) => {
  updateInfo(req, res);
});

router.delete('/api/trucks/:id', (req, res) => {
  deleteTruck(req, res);
});

router.post('/api/trucks/:id/assign', (req, res) => {
  assignTruck(req, res);
});

module.exports = router;
