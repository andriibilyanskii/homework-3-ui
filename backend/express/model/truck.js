const mongoose = require('mongoose');

const trucksSchema = new mongoose.Schema({
  created_by: {
    type: String,
    require: true,
    trim: true,
  },
  assigned_to: {
    type: String,
    require: true,
    trim: true,
  },
  type: {
    type: String,
    enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
    require: true,
    trim: true,
  },
  status: {
    type: String,
    enum: ['OL', 'IS'],
    require: true,
    trim: true,
  },
  created_date: {
    type: String,
    require: true,
    trim: true,
  },
});

const Trucks = mongoose.model('Truck', trucksSchema);

module.exports = Trucks;
