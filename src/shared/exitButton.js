export default function Exit() {
  return (
    <button
      onClick={() => {
        window.localStorage.removeItem('user');
        window.location.pathname = '';
      }}
      className="btn btn-danger"
    >
      Exit
    </button>
  );
}
