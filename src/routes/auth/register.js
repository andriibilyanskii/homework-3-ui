import loginUser from './login';

export default function registerUser(state) {
    fetch('http://localhost:5555/api/auth/register', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
      body: JSON.stringify({
        email: state.email,
        password: state.password,
        role: state.role,
      }),
    })
      .then((res) => res.ok)
      .then((data) => {
        if (data) {
          loginUser(state);
        } else {
          alert('This account is already created');
          window.location.reload();
        }
      })
      .catch((error) => console.log(error));
}