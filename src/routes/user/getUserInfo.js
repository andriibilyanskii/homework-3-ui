export default function getUserInfo(jwt_token) {
  fetch('http://localhost:5555/api/users/me', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json;charset=utf-8',
      Authorization: `Bearer ${jwt_token}`,
    },
  })
    .then((res) => res.json())
    .then((data) => {
      window.localStorage.setItem(
        'user',
        JSON.stringify({
          ...JSON.parse(window.localStorage.getItem('user')),
          created_date: data.created_date,
          role: data.role,
        }),
      );
      console.log(data)
    })
    .catch((error) => console.log(error));
}
