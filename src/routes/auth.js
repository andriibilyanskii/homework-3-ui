import React, {Component} from 'react';
import 'bootstrap';
import registerUser from './auth/register';
import loginUser from './auth/login';

class Auth extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      role: 'DRIVER',
      toRegister: false,
    };

    this.changeInput = this.changeInput.bind(this);
    this.submitForm = this.submitForm.bind(this);
    this.toRegister = this.toRegister.bind(this);
    this.register = this.register.bind(this);
    this.login = this.login.bind(this);

    this.authRef = React.createRef();
  }

  componentDidMount() {
    this.authRef.current.focus();
  }

  changeInput(e) {
    this.setState({[e.target.name]: e.target.value});
  }

  submitForm(e) {
    e.preventDefault();
    if (this.state.email && this.state.password) {
      if (this.state.toRegister) {
        this.register();
      } else {
        this.login();
      }
    }
  }

  register() {
    registerUser(this.state);
  }

  login() {
    loginUser(this.state);
  }

  toRegister() {
    this.setState({toRegister: true});
  }

  render() {
    return (
      <div className="auth">
        <form onSubmit={this.submitForm}>
          <div className="inputs">
            <input
              type="email"
              name="email"
              placeholder="Email"
              onChange={this.changeInput}
              required={true}
              ref={this.authRef}
            />
            <input
              type="password"
              name="password"
              placeholder="Password"
              onChange={this.changeInput}
              required={true}
            />
            <br />
            <select onChange={this.changeInput} name="role" value={this.state.role}>
              <option value="DRIVER">Driver</option>
              <option value="SHIPPER">Shipper</option>
            </select>
          </div>
          <div className="buttons">
            <button type="submit" className="btn btn-primary">
              Login
            </button>
            <button
              type="submit"
              onClick={this.toRegister}
              className="btn btn-warning"
            >
              Haven't an account? Register?
            </button>
          </div>
        </form>
      </div>
    );
  }
}

export default Auth;
