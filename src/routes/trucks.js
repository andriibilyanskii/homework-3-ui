import React, {Component} from 'react';
import formatDate from '../shared/formatDate';
import Exit from '../shared/exitButton';
class Trucks extends Component {
  constructor(props) {
    super(props);
    this.state = {
      notes: {notes: []},
      text: '',
      user: localStorage.getItem('user')
        ? JSON.parse(localStorage.getItem('user'))
        : {email: '', jwt_token: ''},
    };

    this.getAllNotes = this.getAllNotes.bind(this);
    this.createNote = this.createNote.bind(this);
    this.changeInput = this.changeInput.bind(this);
    this.changeNoteState = this.changeNoteState.bind(this);
    this.updateNote = this.updateNote.bind(this);
    this.deleteNote = this.deleteNote.bind(this);

    this.inputTextRef = React.createRef();
  }

  componentDidMount() {
    if (this.state.user.email) {
      this.getAllNotes();
    }
    this.inputTextRef.current.focus();
  }

  getAllNotes() {
    fetch('http://localhost:5555/api/trucks', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
        Authorization: `Bearer ${this.state.user.jwt_token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => this.setState({notes: data}))
      .catch((error) => console.log(error));
  }

  createNote(e) {
    e.preventDefault();
    fetch('http://localhost:5555/api/notes', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
        Authorization: `Bearer ${this.state.user.jwt_token}`,
      },
      body: JSON.stringify({
        userId: this.state.user.userId,
        completed: false,
        text: this.state.text,
        createdDate: new Date().toISOString(),
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        this.getAllNotes();
        this.setState({text: ''});
      })
      .catch((error) => console.log(error));
  }

  changeInput(e) {
    this.setState({[e.target.name]: e.target.value});
  }

  changeNoteState(noteId) {
    fetch('http://localhost:5555/api/notes/' + noteId, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
        Authorization: `Bearer ${this.state.user.jwt_token}`,
      },
    })
      .then((res) => res.json())
      .then(() => this.getAllNotes())
      .catch((error) => console.log(error));
  }

  updateNote(elem, noteId) {
    fetch('http://localhost:5555/api/notes/' + noteId, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
        Authorization: `Bearer ${this.state.user.jwt_token}`,
      },
      body: JSON.stringify({text: elem.target.value}),
    })
      .then((res) => res.json())
      .then((data) => {
        this.setState({response: data});
        this.getAllNotes();
      })
      .catch((error) => console.log(error));
  }

  deleteNote(noteId) {
    fetch('http://localhost:5555/api/notes/' + noteId, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
        Authorization: `Bearer ${this.state.user.jwt_token}`,
      },
    })
      .then((res) => res.json())
      .then(() => this.getAllNotes())
      .catch((error) => console.log(error));
  }

  render() {
    return (
      <div>
        <div className="top-buttons">
          <button
            onClick={() => (window.location.pathname = '/user')}
            className="btn btn-primary"
          >
            Profile info
          </button>
          <Exit />
        </div>
        <div className="notes">
          <table
            style={{
              display:
                (this.state.notes.notes || []).length !== 0 ? 'initial' : 'none',
            }}
          >
            <thead>
              <tr>
                <td>Text</td>
                <td>Completed</td>
                <td>Creation date</td>
                <td></td>
              </tr>
            </thead>
            <tbody>
              {(this.state.notes.notes || []).map((e) => (
                <tr key={e._id}>
                  <td>
                    <input
                      defaultValue={e.text}
                      onBlur={(elem) => this.updateNote(elem, e._id)}
                    />
                  </td>
                  <td>
                    <button
                      onClick={() => this.changeNoteState(e._id)}
                      className={
                        'btn ' + (e.completed ? 'btn-primary' : 'btn-warning')
                      }
                    >
                      {e.completed ? 'Completed' : 'Not completed'}
                    </button>
                  </td>
                  <td>{formatDate(new Date(e.createdDate))}</td>
                  <td>
                    <button
                      onClick={() => this.deleteNote(e._id)}
                      className="btn btn-danger"
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
        <div className="create">
          <form onSubmit={this.createNote}>
            <input
              type="text"
              name="text"
              onChange={this.changeInput}
              placeholder="Enter text....."
              value={this.state.text}
              required={true}
              ref={this.inputTextRef}
            />
            <button type="submit" className="btn btn-success">
              Add note
            </button>
          </form>
        </div>
      </div>
    );
  }
}

export default Trucks;
